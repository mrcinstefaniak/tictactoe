window.addEventListener("load", init, false);
var a, b, who, na, nb, x, y, turn, turnCount, i, j, you, him;
var tiles;
var table = [[], [], []];
var o = "url(o.svg)"
var k = "url(x.svg)"
var player = k;
var ai = o;
var difficulty = 3;

function winf()
{
  var y, x;
  var w = table[a][b]
  for(x=0;x<4;x++)
  {
    if(x==3) return 1;
    if(table[a][x]!=w) break;
  }
  for(y=0;y<4;y++)
  {
    if(y==3) return 1;
    if(table[y][b]!=w) break;
  }
  for(y=0;y<4;y++)
  {
    if(y==3) return 1;
    if(table[y][-y+2]!=w) break;
  }
  for(y=0;y<4;y++)
  {
    if(y==3) return 1;
    if(table[y][y]!=w) break;
  }
}

function end(who)
{
  if(who=="player") you++;
  else if(who=="computer") him++;
  document.getElementById("you").getElementsByTagName("span")[0].innerHTML = you;
  document.getElementById("you").getElementsByTagName("span")[1].innerHTML = you;
  document.getElementById("him").getElementsByTagName("span")[0].innerHTML = him;
  document.getElementById("him").getElementsByTagName("span")[1].innerHTML = him;
  setTimeout(reset, 2000);
}
function reset()
{
  table = [[], [], []];
  turn = "none";
  draw();
  game();
}

function draw()
{
  for(var y=0; y<3; y++)
  {
    for(var x=0; x<3; x++)
    {
      if(table[y][x])
      {
        tiles[(x*3)+y].firstElementChild.style.mask = table[y][x];
        tiles[(x*3)+y].firstElementChild.style.WebkitMask = table[y][x];
        tiles[(x*3)+y].firstElementChild.style.width = "100%";
        tiles[(x*3)+y].firstElementChild.style.height = "100%";
        tiles[(x*3)+y].firstElementChild.style.margin = "0";
      }
      else
      {
        tiles[(x*3)+y].firstElementChild.style.width = "0";
        tiles[(x*3)+y].firstElementChild.style.height = "0";
        tiles[(x*3)+y].firstElementChild.style.margin = "50%";
      }
    }
  }
}

function computerDecision()
{
  a = na;
  b = nb;
  table[a][b] = ai;
  draw();
  if(winf()) {end("computer"); return}
  if(turnCount > 4) {end("noone"); return;}
  else turn = "player";
}

function computer()
{
  if(turnCount > 4 && player == k) {end("noone"); return;}
  if(difficulty > 1)
  {
    {
      for(y=0; y<3; y++)
      {
        i=0;
        for(x=0; x<3; x++)
        {
          if(table[y][x]!=ai)
          {
            i++;
            if(table[y][x]==player) i++;
            na = y;
            nb = x;
          }
        }
        if(i==1) {computerDecision(); return;};
      }
      for(x=0; x<3; x++)
      {
        i=0;
        for(y=0; y<3; y++)
        {
          if(table[y][x]!=ai)
          {
            i++;
            if(table[y][x]==player) i++;
            na = y;
            nb = x;
          }
        }
        if(i==1) {computerDecision(); return;};
      }
      i=0;
      for(y=0; y<3; y++)
      {
        if(table[y][y]!=ai)
        {
          i++;
          if(table[y][y]==player) i++;
          na = y;
          nb = y;
        }
      }
      if(i==1) {computerDecision(); return;};
      i=0;
      for(y=0; y<3; y++)
      {
        if(table[y][-y+2]!=ai)
        {
          i++;
          if(table[y][-y+2]==player) i++;
          na = y;
          nb = -y+2;
        }
      }
      if(i==1) {computerDecision(); return;};
    }
  }

  if(difficulty > 2)
  {
    for(y=0; y<3; y++)
    {
      i=0;
      j=0;
      for(x=0; x<3; x++)
      {
        if(table[y][x]==player) i++;
        if(!table[y][x])
        {
          j++;
          na = y;
          nb = x;
        }
      }
      if(i==2&&j==1) {computerDecision(); return;};
    }
    for(x=0; x<3; x++)
    {
      i=0;
      j=0;
      for(y=0; y<3; y++)
      {
        if(table[y][x]==player) i++;
        if(!table[y][x])
        {
          j++;
          na = y;
          nb = x;
        }
      }
      if(i==2&&j==1) {computerDecision(); return;};
    }
    i=0;
    j=0;
    for(y=0; y<3; y++)
    {
      if(table[y][y]==player) i++;
      if(!table[y][y])
      {
        j++;
        na = y;
        nb = y;
      }
    }
    if(i==2&&j==1) {computerDecision(); return;};
    i=0;
    j=0;
    for(y=0; y<3; y++)
    {
      if(table[y][-y+2]==player) i++;
      if(!table[y][-y+2])
      {
        j++;
        na = y;
        nb = -y+2;
      }
    }
    if(i==2&&j==1) {computerDecision(); return;};
  }

  if(difficulty > 3)
  {
    if(turnCount==1)
    {
      if(!table[0][0])
      {
        na = 0;
        nb = 0;
      }
      if(!table[0][2])
      {
        na = 0;
        nb = 2;
      }
      computerDecision();
      return;
    }
    if(turnCount==2)
    {
      for(y=0; y<3; y+=2)
      {
        for(x=0; x<3; x+=2)
        {
          if(x==y || x==-y+2)
          {
            if(table[y][x]==ai && !table[-y+2][-x+2])
            {
              na = -y+2;
              nb = -x+2;
              computerDecision();
              return;
            }
            else if(table[y][x]==ai && !table[-y+2][x])
            {
              na = -y+2;
              nb = x;
              computerDecision();
              return;
            }
          }
        }
      }
    }
    if(turnCount==3)
    {
      for(y=0; y<3; y+=2)
      {
        for(x=0; x<3; x+=2)
        {
          if(x==y || x==-y+2)
          {
            if(table[y][x] == ai && table[y][-x+2] == ai && !table[-y+2][x])
            {
              console.log("test");
              na = -y+2;
              nb = x;
              computerDecision();
              return;
            }
            if(table[y][x] == ai && table[-y+2][x] == ai && !table[y][-x+2])
            {
              console.log("test");
              na = y;
              nb = -x+2;
              computerDecision();
              return;
            }
          }
        }
      }
    }
  }
  do
  {
    na = Math.floor((Math.random()*10)%3);
    nb = Math.floor((Math.random()*10)%3);
  } while (table[na][nb]);
  computerDecision();
}

function playerTurn()
{
  if(turn == "player")
  {
    a = this.id%3;
    b = Math.floor(this.id/3);
    turnCount++;
    if(!table[a][b]) table[a][b] = player;
    else return;
    turn = "computer";
    draw();
    if(winf()) {end("player"); return;}
    else setTimeout(computer, 200);
  }
}

function menuShow()
{
  document.getElementsByClassName("game-menu")[0].style.opacity = "1";
  document.getElementsByClassName("game-menu")[0].style.zIndex = "1";
  document.getElementsByClassName("game-main")[0].style.opacity = "0.55";
  document.getElementsByClassName("game-main")[0].style.zIndex = "-1";
}

function menuClose()
{
  document.getElementsByClassName("game-menu")[0].style.opacity = "0";
  document.getElementsByClassName("game-menu")[0].style.zIndex = "-1";
  document.getElementsByClassName("game-main")[0].style.opacity = "1";
  document.getElementsByClassName("game-main")[0].style.zIndex = "1";
}

function orderSet()
{
  if(player == k)
  {
    document.getElementById("option-order").getElementsByTagName("span")[0].innerHTML = "Komputer";
    document.getElementById("option-order").getElementsByTagName("span")[1].innerHTML = "Computer";
    player = o;
    ai = k;
  }
  else
  {
    document.getElementById("option-order").getElementsByTagName("span")[0].innerHTML = "Ty";
    document.getElementById("option-order").getElementsByTagName("span")[1].innerHTML = "You";
    player = k;
    ai = o;
  }
  reset();
}

function difficultySet()
{
  var pl = document.getElementById("option-difficulty").getElementsByTagName("span")[0];
  var en = document.getElementById("option-difficulty").getElementsByTagName("span")[1];
  if(difficulty == 4) difficulty = 1;
  else difficulty++;

  switch (difficulty)
  {
    case 1:
      pl.innerHTML = "Losowy";
      en.innerHTML = "Random";
      break;
    case 2:
        pl.innerHTML = "Łatwy";
        en.innerHTML = "Easy";
        break;
    case 3:
        pl.innerHTML = "Średni";
        en.innerHTML = "Medium";
        break;
    case 4:
      pl.innerHTML = "Trudny";
      en.innerHTML = "Hard";
      break;
  }
  reset();
}

function game()
{
  turnCount = 0;
  if(player == k) turn = "player";
  else {turnCount++; computer();}
}

function init()
{
  turn = "none";
  you = 0;
  him = 0;
  tiles = document.getElementsByClassName("game-tile");
  document.getElementById("option-switch").addEventListener("click", menuShow, false);
  document.getElementById("option-return").addEventListener("click", menuClose, false);
  document.getElementById("option-reset").addEventListener("click", reset, false);
  document.getElementById("option-order").addEventListener("click", orderSet, false);
  document.getElementById("option-difficulty").addEventListener("click", difficultySet, false);
  for(var i=0; i<tiles.length; i++)
  {
    tiles[i].id = i;
    tiles[i].addEventListener("click", playerTurn, false);
  }
  game();
}
