#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
//32 - space; 88- X; 79 - O;
int ai(int * a, int * b, char table[3][3], char order, int difficulty, int runda)
{
  int na, nb, x, y, i=0, j=0;
  if(difficulty>1)
  {
    for(y=0; y<3; y++)
    {
      i=0;
      for(x=0; x<3; x++)
      {
        if(table[y][x]!=88)
        {
          i++;
          if(table[y][x]==79) i++;
          na = y;
          nb = x;
        }
      }
      if(i==1) goto przypis;
    }
    for(x=0; x<3; x++)
    {
      i=0;
      for(y=0; y<3; y++)
      {
        if(table[y][x]!=88)
        {
          i++;
          if(table[y][x]==79) i++;
          na = y;
          nb = x;
        }
      }
      if(i==1) goto przypis;
    }
    i=0;
    for(y=0; y<3; y++)
    {
      if(table[y][y]!=88)
      {
        i++;
        if(table[y][y]==79) i++;
        na = y;
        nb = y;
      }
    }
    if(i==1) goto przypis;
    i=0;
    for(y=0; y<3; y++)
    {
      if(table[y][-y+2]!=88)
      {
        i++;
        if(table[y][-y+2]==79) i++;
        na = y;
        nb = -y+2;
      }
    }
    if(i==1) goto przypis;
  }
  if(difficulty>2)
  {
    printf("hard\n");
    for(y=0; y<3; y++)
    {
      i=0;
      j=0;
      for(x=0; x<3; x++)
      {
        if(table[y][x]==79) i++;
        if(table[y][x]==32)
        {
          j++;
          na = y;
          nb = x;
        }
      }
      if(i==2&&j==1) goto przypis;
    }
    for(x=0; x<3; x++)
    {
      i=0;
      j=0;
      for(y=0; y<3; y++)
      {
        if(table[y][x]==79) i++;
        if(table[y][x]==32)
        {
          j++;
          na = y;
          nb = x;
        }
      }
      if(i==2&&j==1) goto przypis;
    }
    i=0;
    j=0;
    for(y=0; y<3; y++)
    {
      if(table[y][y]==79) i++;
      if(table[y][y]==32)
      {
        j++;
        na = y;
        nb = y;
      }
    }
    if(i==2&&j==1) goto przypis;
    i=0;
    j=0;
    for(y=0; y<3; y++)
    {
      if(table[y][-y+2]==79) i++;
      if(table[y][-y+2]==32)
      {
        j++;
        na = y;
        nb = -y+2;
      }
    }
    if(i==2&&j==1) goto przypis;
  }
  if(difficulty>3)
  {
    if(runda==1)
    {
      if(table[0][0]==32)
      {
        na = 0;
        nb = 0;
      }
      if(table[0][2]==32)
      {
        na = 0;
        nb = 2;
      }
      goto przypis;
    }
    if(runda==2)
    {
      for(y=0; y<3; y+=2)
      {
        for(x=0; x<3; x+=2)
        {
          if(x==y || x==-y+2)
          {
            if(table[y][x]==88 && table[-y+2][-x+2]==32)
            {
              na = -y+2;
              nb = -x+2;
              goto przypis;
            }
          }
        }
      }
    }
  }
  do {
    printf("random\n");
    na = rand()%3;
    nb = rand()%3;
  } while(table[na][nb] != 32);
  przypis:
  *a = na;
  *b = nb;
  return 0;
}

void player(int * a, int * b, char table[3][3])
{
  int na, nb;
  do {
    printf("Y: ");
    scanf("%d", &na);
    printf("X: ");
    scanf("%d", &nb);
    if(table[na][nb] != 32) printf("Zajete!\n");
  } while(table[na][nb] != 32);
  *a = na;
  *b = nb;
}

int winf(char table[3][3], int a, int b)
{
  int y, x, w=table[a][b];
  for(x=0;x<4;x++)
  {
    if(x==3) return table[a][b];
    if(table[a][x]!=w) break;
  }
  for(y=0;y<4;y++)
  {
    if(y==3) return table[a][b];
    if(table[y][b]!=w) break;
  }
  for(y=0;y<4;y++)
  {
    if(y==3) return table[a][b];
    if(table[y][-y+2]!=w) break;
  }
  for(y=0;y<4;y++)
  {
    if(y==3) return table[a][b];
    if(table[y][y]!=w) break;
  }
  return 0;
}

void clear(int a)
{
  printf("\n");
  if(a>1) clear(a-1);
}

void draw(char table[3][3])
{
  for(int y=0; y<3; y++)
  {
    if(y==0) {printf("\\|0|1|2|\n"); printf("-+-+-+-+\n");}
    for(int x=0; x<3; x++)
    {
      if(x==0) printf("%d|", y);
      printf("%c|", table[y][x]);
    }
    printf("\n-+-+-+-+\n");
    if(y==2) printf("\n\n");
  }
}
int game(int difficulty, char order)
{
  int a=0, b=0, win=0, runda=0;
  char table[3][3]={"   ","   ","   "};

  clear(50);
  do
  {
    runda++;
    draw(table);
    if(order == 116)
    {
      player(&a, &b, table);
      table[a][b] = 79;
    }
    else
    {
      ai(&a, &b, table, order, difficulty, runda);
      table[a][b] = 88;
    }
    clear(50);
    draw(table);
    win = winf(table, a, b);
    if(!win)
    {
      for(int i = 0; i<3; i++)
      {
        for(int j = 0; j<3; j++) {if(table[i][j] == 32) i=3;}
        if(i == 3) break;
        if(i == 2)
        {
          clear(50);
          draw(table);
          printf("Remis!\n");
          return 0;
        }
      }
    }
    if(!win)
    {
      if(order == 116)
      {
        ai(&a, &b, table, order, difficulty, runda);
        table[a][b] = 88;
      }
      else
      {
        player(&a, &b, table);
        table[a][b] = 79;
      }
      clear(50);
      draw(table);
      win = winf(table, a, b);
    }
    clear(50);
  } while(!win);
  draw(table);
  if(win==88) printf("Niestety, przegrałeś.\n");
  if(win==79) printf("Brawo! Wygraleś!\n");
  return 0;
}
int main(void)
{
  int difficulty;
  char play, order;
  srand(time(NULL));
  do {
    clear(50);
    printf("Podaj poziom trudności:\n1: losowy\n2: pół-losowy\n3: pół-losowy z blokowaniem\n4: idealny, nie wygrasz(work in progress)\n");
    do
    {
      printf("Poziom: ");
      scanf("%d", &difficulty);
      if(difficulty < 1 || difficulty > 4) printf("Nie ma takiego poziomu trudności!\n");
    }while(difficulty < 1 || difficulty > 4);
    printf("Chcesz zaczynać?(napisz \"t\", jeśli chcesz): ");
    scanf(" %c", &order);
    game(difficulty, order);
    printf("Chcesz zagrać jeszcze raz?(napisz \"t\", jeśli chcesz): ");
    scanf(" %c", &play);
  } while(play == 116);
  printf("Koniec gry.");
  return 0;
}
