window.addEventListener("load", lang_init, false);

function lang_init()
{
  var lang_sel = document.getElementsByClassName("language");
  var lang = document.getElementsByClassName("lang");
  language(lang_sel, lang);
  for(var i = 0; i < lang_sel.length; i++) lang_sel[i].addEventListener("click",function(){language(lang_sel, lang);}, false);
}

function language(lang_sel, lang)
{
  var l;
  for(var i = 0; i < lang_sel.length; i++)
  {
    if(lang_sel[i].children[0].checked) l=lang_sel[i].children[0].value;
  }
  l = document.getElementsByClassName(l);
  for(var i = 0; i < lang.length; i++) lang[i].style.display = "none";
  for(var i = 0; i < l.length; i++) l[i].style.display = "initial";
}
